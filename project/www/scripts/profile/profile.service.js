(function () {
	'use strict';

	angular
		.module('restaurant.profile')
		.factory('profileService', profileService)
		.filter('capitalize', capitalize)
	profileService.$inject = ['dataService', '$firebaseObject', '$q'];

	/* @ngInject */
	function profileService(dataService, $firebaseObject, $q) {
		var service = {
			getFeaturedCategories: getFeaturedCategories,
			getFeaturedProducts: getFeaturedProducts,
			getBusiness: dataService.getBusiness,
			getCuisines: getCuisines,
			syncProfileData: syncProfileData,
			saveProfileChanges: saveProfileChanges,
			getStorageBusiness: getStorageBusiness,
			updateStore: updateStore,
			geocodeLatLng: geocodeLatLng,
		};
		return service;

		// ***************************************************************

		function getFeaturedCategories() {
			return dataService.getFeaturedCategories();
		}

		function getCuisines() {
			return dataService.getCuisines();
		}

		function getFeaturedProducts() {
			return dataService.getFeaturedProducts();
		}

		function syncProfileData(userId) {
			return dataService.syncProfileData(userId);
		}

		function saveProfileChanges(copyDataObj, firebaseObj) {
			return dataService.saveProfileChanges(copyDataObj, firebaseObj);
		}

		function getStorageBusiness(businessId, imageData, directory, name) {
			return dataService.getStorageBusiness(businessId, imageData, directory, name)
		}

		function updateStore(businessId, name) {
			return dataService.updateStore(businessId, name)
		}

		function geocodeLatLng(lat, long) {
			return $q(function (resolve, reject) {
				var geocoder = new google.maps.Geocoder;
				var latlng = { lat: parseFloat(lat), lng: parseFloat(long) };
				console.info("latlng", latlng)
				geocoder.geocode({ 'location': latlng }, function (results, status) {
				console.info("status", status);
				console.info(results);
					if (status === 'OK') {
						if (results[0]) {
							resolve(results[0].formatted_address);
						} else {
							reject('No results found');
							console.info("No results found");
						}
					} else {
						reject('Geocoder failed due to: ' + status);
						if (window.cordova) {
							//noinspection JSUnresolvedFunction
							cordova.plugins.snackbar('Geocoder failed due to: ' + status, 'LONG', "", function () { });
						} else { alert('Geocoder failed due to: ' + status) }
					}
				});
			});
		}

	}

	function capitalize() {
		return function (input, all) {
			return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			}) : '';
		}
	}

})();
